package github;

import java.io.IOException;
import java.util.Map;

import org.kohsuke.github.GHEventInfo;
import org.kohsuke.github.GHRepository;
import org.kohsuke.github.GHUser;
import org.kohsuke.github.GitHub;
import org.kohsuke.github.PagedIterable;
import org.kohsuke.github.PagedIterator;

public class Main {
	public static void main(String[] args) throws IOException {
		GitHub github = GitHub.connectAnonymously();
		GitHub.connectUsingOAuth("ここにトークンを入力");
//		GitHub github = GitHub.connect();
//		System.out.println(github.createRepository("new-tom-repo", "this is my new repository", "https://github.com/tomdog2", true));
		GHUser user = github.getUser("tomdog2");
		String userName = user.getName();
		String userLogin = user.getLogin();
		System.out.println("user_name : " + userName);
		System.out.println("userLogin : " + userLogin);
		Map<String, GHRepository> repositories = user.getRepositories();
		PagedIterable<GHEventInfo> listEvents = user.listEvents();
		PagedIterator<GHEventInfo> iterator = listEvents.iterator();
		while(iterator.hasNext()) {
			System.out.println(iterator.next().getCreatedAt());
		}
	}
}
